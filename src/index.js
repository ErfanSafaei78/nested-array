import { data } from './data.js';

function findChildren (parentObj) {
  data.forEach(obj => {
    if (obj.parentId === parentObj.id ){
      if(parentObj.children && parentObj.childrenIds){
        parentObj.children.push(obj);
        parentObj.childrenIds.push(obj.id);
      }
      else {
        parentObj.children = [obj];
        parentObj.childrenIds = [obj.id];
      }
      findChildren(parentObj.children[parentObj.children.length -1])
    };
  })
};

function buildTree() {
  let tree = [];
  const root = data.filter(el => el.parentId === null);
  if (root.length){
    tree = root;
  }
  else {
    let i= 0;
    data.forEach(obj => {
      if (obj.parentId < i){
        i = obj.parentId;
      }
    });
    data.forEach (obj => {
      if(obj.parentId === i) {
        tree.push(obj);
      };
    });
  };
  
  tree.forEach (obj => {
      findChildren(obj);
    });
  return tree;
};

console.log(buildTree());
