<?php

$data = [
    [
        "name" => "obj01",
        "id" => 1,
        "parentId" => null, 
    ],
    [
        "name" => "obj02",
        "id" => 2,
        "parentId" => null, 
    ],
    [
        "name" => "obj03",
        "id" => 3,
        "parentId" => 1, 
    ],
    [
        "name" => "obj04",
        "id" => 4,
        "parentId" => 1, 
    ],
    [
        "name" => "obj05",
        "id" => 5,
        "parentId" => 2, 
    ],
    [
        "name" => "obj06",
        "id" => 6,
        "parentId" => 3, 
    ],
    [
        "name" => "obj07",
        "id" => 7,
        "parentId" => 6, 
    ],
    [
        "name" => "obj08",
        "id" => 8,
        "parentId" => 4, 
    ],
    [
        "name" => "obj09",
        "id" => 9,
        "parentId" => 3, 
    ],
    [
        "name" => "obj10",
        "id" => 10,
        "parentId" => 7, 
    ],
    [
        "name" => "obj11",
        "id" => 11,
        "parentId" => 10, 
    ],
    [
        "name" => "obj12",
        "id" => 12,
        "parentId" => 11, 
    ],
    [
        "name" => "obj13",
        "id" => 13,
        "parentId" => 12, 
    ],
    [
        "name" => "obj14",
        "id" => 14,
        "parentId" => 13, 
    ],
    [
        "name" => "obj15",
        "id" => 15,
        "parentId" => 14, 
    ],
    [
        "name" => "obj16",
        "id" => 16,
        "parentId" => 15, 
    ],
    [
        "name" => "obj17",
        "id" => 17,
        "parentId" => 16, 
    ],
    [
        "name" => "obj18",
        "id" => 18,
        "parentId" => 17, 
    ],
    [
        "name" => "obj19",
        "id" => 19,
        "parentId" => 18, 
    ],
    [
        "name" => "obj20",
        "id" => 20,
        "parentId" => 19, 
    ],
    [
        "name" => "obj21",
        "id" => 21,
        "parentId" => 6, 
    ],
    [
        "name" => "obj22",
        "id" => 22,
        "parentId" => 14, 
    ],
    [
        "name" => "obj23",
        "id" => 23,
        "parentId" => 29, 
    ],
    [
        "name" => "obj24",
        "id" => 24,
        "parentId" => 9, 
    ],
    [
        "name" => "obj25",
        "id" => 25,
        "parentId" => 19, 
    ],
    [
        "name" => "obj26",
        "id" => 26,
        "parentId" => 30, 
    ],
    [
        "name" => "obj27",
        "id" => 27,
        "parentId" => 8, 
    ],
    [
        "name" => "obj28",
        "id" => 28,
        "parentId" => 9, 
    ],
    [
        "name" => "obj29",
        "id" => 29,
        "parentId" => 17, 
    ],
    [
        "name" => "obj30",
        "id" => 30,
        "parentId" => 13, 
    ],
]

?>