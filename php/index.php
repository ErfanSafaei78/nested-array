<?php
include './data.php';


function findChildren($data, $parentObj){
        foreach($data as $obj){
                if($obj["parentId"] === $parentObj["id"]){
                        if($parentObj["children"] && $parentObj["childrenIds"]) {
                                array_push($parentObj["children"],$obj);
                                array_push($parentObj["childrenIds"],$obj["id"]);
                        }
                        else {
                                $parentObj["children"] = [$obj];
                                $parentObj["childrenIds"] = [$obj["id"]];
                        };
                        findChildren($data, $parentObj["children"][count ($parentObj["children"]) -1]);
                        // var_dump($parentObj);
                };
        };
};


function buildTree($data) {
        $tree = [];
        $root = [];
        foreach($data as $obj) {
                if ($obj["parentId"] === null) {
                        array_push($root,$obj);
                };
        };
        if(count($root)){
                $tree = $root;
        }
        else {
                $i = 0;
                foreach($data as $obj){
                        if($obj["parentId"] < $i) {
                                $i = $obj["parentId"];
                        };
                };
                foreach($data as $obj){
                        if($obj["parentId"] === $i){
                                array_push($tree, $obj);
                        };
                };
        };
        foreach($tree as $obj){
                findChildren($data ,$obj);
        };
        return $tree;
};
var_dump( buildTree($data) );
?>
